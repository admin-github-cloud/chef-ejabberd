# Creating Cookbooks With TDD

Chef-dk
↓
TestKitchen
Berkshelf
ChefSpec
ServerSpec

---

# Shane Sveller
Jr. System Engineer at Belly, Inc.

---

# Set Up

```bash
$ brew tap phinze/cask
$ brew install brew-cask
$ brew cask install chef-dk
```

---

Let's create a Chef cookbook to install the `ejabberd` XMPP server, based on Linode's [excellent community guide](https://library.linode.com/communications/xmpp/ejabberd/ubuntu-12.04-precise-pangolin).

---

# Getting Started

```bash
$ chef generate cookbook ejabberd
Compiling Cookbooks...
Recipe: code_generator::cookbook
  * directory[~/src/ejabberd] action create
    - create new directory [...]
[...]
```

---

# What platforms will be supported?

* Ubuntu 12.04 LTS
* Ubuntu 14.04 LTS

---

# Where to start?

---

Tests, of course!

---

# ChefSpec

* *analyzes* chef recipes
* *simulates* the resultant chef run
* does *not* actually make changes to any system
* makes assertions about what the chef run *should* or *should not* do
* *much* faster than *vagrant*-based testing

---

ChefSpec is great for *unit* tests, but only *supplements* E2E testing

---

# Install ChefSpec

```bash
$ bundle init && $EDITOR Gemfile && bundle install
```

```ruby
# Gemfile
source "https://rubygems.org"
gem "berkshelf", "~>3.0"
gem "chefspec", "~>3.0"
```

---

# First spec

```ruby
# spec_helper.rb
require 'chefspec'
require 'chefspec/berkshelf'

# default_spec.rb
require_relative 'spec_helper'

describe 'ejabberd::default' do
  it 'installs ejabberd'
end
```

---

# Simulate the Chef run

```ruby
subject do
  runner = ChefSpec::Runner.new
  runner.converge(described_recipe)
  # from: describe 'ejabberd::default'
end
```

---

# Make an assertion

```ruby
describe 'ejabberd::default' do
  it 'installs ejabberd' do
    expect(subject).to install_package('ejabberd')
  end
end
```

---

# Watch it fail

```bash
$ rspec spec/
F

Failures:
  1) ejabberd::default installs ejabberd
     Failure/Error: expect(subject).to install_package('ejabberd')
       expected "package[ejabberd]" with ↩
       action :install to be in Chef run.

Finished in 0.02866 seconds
1 example, 1 failure
```

---

# Make it pass

```ruby
# recipes/default.rb
package 'ejabberd'
```

↓

```bash
$ rspec spec/
.

Finished in 0.0298 seconds
1 example, 0 failures
```

---

# ✔ Great success!

---

# Always install the latest

```ruby
it 'installs ejabberd' do
  expect(subject).to upgrade_package('ejabberd')
end
```
↓

```bash
Failures:

  1) ejabberd::default installs ejabberd
     Failure/Error: expect(subject).to upgrade_package('ejabberd')
       expected "package[ejabberd]" actions [:install] to include :upgrade
```

---

# Fix it!

---

```ruby
package 'ejabberd' do
  action :upgrade
end
```
↓

```bash
$ rspec spec/
.

Finished in 0.02936 seconds
1 example, 0 failures
```

---

# Suggestion

Accelerate the feedback loop with `guard-rspec`

```ruby
# Gemfile
gem "guard-rspec"

# Guardfile
guard :rspec, cmd: 'bundle exec rspec' do
  watch(%r{^spec/.+_spec\.rb$})
  watch(%r{^recipes/(.+)\.rb$}) { |m| "spec/#{m[1]}_spec.rb" }
  watch('spec/spec_helper.rb')  { 'spec' }
end
```

---

That's great for unit testing, but what about integration tests?

---

# [fit] TestKitchen

---

# Installation

```ruby
# Gemfile
gem "test-kitchen"
gem "kitchen-vagrant"
# gem "kitchen-cloudstack"
# gem "kitchen-digitalocean"
# gem "kitchen-docker"
# gem "kitchen-ec2"
# gem "kitchen-joyent"
# gem "kitchen-openstack"
# gem "kitchen-rackspace"

```

---

# Configuration

#`.kitchen.yml`
```
platforms:
  # From opscode/bento
  - name: ubuntu-12.04
  - name: ubuntu-14.04
suites:
  - name: default
    run_list:
      - recipe[ejabberd::default]
```

[opscode/bento](https://github.com/opscode/bento): Vagrant boxes with no built-in provisioner

---

# Silence some warnings

##`.kitchen.yml`
```
provisioner:
  name: chef_solo

  solo_rb:
    ssl_verify_mode: verify_peer
```

---

# Listing test suites

```bash
$ kitchen list
Instance             Driver   Provisioner  Last Action
default-ubuntu-1204  Vagrant  ChefSolo     <Not Created>
default-ubuntu-1404  Vagrant  ChefSolo     <Not Created>
```

One instance defined for each *platform* and *suite* combination.

---

# Smoke testing

Does the chef run complete without errors?

```bash
$ kitchen test $INSTANCE_REGEX_OR_NAME
```

By default, this command destroys *successful* instances but leaves failures for inspection via:

```bash
$ kitchen login $INSTANCE_REGEX_OR_NAME
```

---

# Sample output

```bash
$ kitchen test 1204
-----> Starting Kitchen (v1.2.1)
-----> Cleaning up any prior instances of <default-ubuntu-1204>
-----> Destroying <default-ubuntu-1204>...
       Finished destroying <default-ubuntu-1204> (0m0.00s).
-----> Testing <default-ubuntu-1204>
-----> Creating <default-ubuntu-1204>...
```

---

# Sample output

```bash
       Vagrant instance <default-ubuntu-1204> created.
       Finished creating <default-ubuntu-1204> (0m32.35s).
-----> Converging <default-ubuntu-1204>...
       Preparing files for transfer
       Resolving cookbook dependencies with Berkshelf 3.1.1...
       Removing non-cookbook files before transfer
       Preparing data bags
       Preparing environments
       Preparing roles
       Preparing encrypted data bag secret
```

---

# Sample output

```bash
-----> Installing Chef Omnibus (true)
       downloading https://www.getchef.com/chef/install.sh
         to file /tmp/install.sh
```

---

# Sample output

```bash
[2014-04-30T19:43:44+00:00] INFO: Forking chef instance to converge...
Starting Chef Client, version 11.12.4
[2014-04-30T19:43:44+00:00] INFO: *** Chef 11.12.4 ***
[2014-04-30T19:43:44+00:00] INFO: Chef-client pid: 1253
[2014-04-30T19:43:46+00:00] INFO: Setting the run_list to ↩
       ["recipe[ejabberd::default]"] from CLI options
[2014-04-30T19:43:46+00:00] INFO: Run List is [recipe[ejabberd::default]]
[2014-04-30T19:43:46+00:00] INFO: Run List expands to [ejabberd::default]
[2014-04-30T19:43:46+00:00] INFO: Starting Chef Run for default-ubuntu-1204
[2014-04-30T19:43:46+00:00] INFO: Running start handlers
[2014-04-30T19:43:46+00:00] INFO: Start handlers complete.
Compiling Cookbooks...
Converging 1 resources
```

---

# Sample output

```bash
Chef Client finished, 1/1 resources updated in 11.204708986 seconds

       Finished converging <default-ubuntu-1204> (0m20.75s).
-----> Setting up <default-ubuntu-1204>...
       Finished setting up <default-ubuntu-1204> (0m0.00s).
-----> Verifying <default-ubuntu-1204>...
       Finished verifying <default-ubuntu-1204> (0m0.00s).
-----> Destroying <default-ubuntu-1204>...
       ==> default: Forcing shutdown of VM...
       ==> default: Destroying VM and associated drives...
       Vagrant instance <default-ubuntu-1204> destroyed.
       Finished destroying <default-ubuntu-1204> (0m3.64s).
       Finished testing <default-ubuntu-1204> (0m59.82s).
-----> Kitchen is finished. (1m0.29s)
```

---

# Integration Testing with TestKitchen

*Busser* gem connects a TestKitchen suite from `.kitchen.yml` to a set of tests that run *on* the virtual machine

TestKitchen supports a few test runners right now, including:

* github.com/sstephenson/bats
* serverspec.org

---

# BATS

Created by the author of rbenv & ruby-build, tests are written in `bash` syntax with some *sugar*:

```bash
#!/usr/bin/env bats

load test_helper

@test "blank invocation" {
  run rbenv
  assert_success
  assert [ "${lines[0]}" = "rbenv 0.4.0" ]
}
```

---

*BATS* is pretty great for testing that a particular command is available on the system after the Chef run completes, i.e. `rvm`

It's less great for verifying that users exist, services are running, etc. which are better handled by...

---

# ServerSpec

Awesome set of RSpec matchers

```ruby
describe file('/etc/sudoers')
describe package('apache2')
describe port(80)
describe process('runit')
describe service('nginx')
describe user('root')
```

Many more at [serverspec.org](http://serverspec.org/resource_types.html)

---

```ruby
describe file('/etc/sudoers') do
  it { should be_readable.by('owner') }
  it { should be_readable.by('group') }
  it { should be_readable.by('others') }
  it { should_not be_writable.by_user('apache') }
end
```

---

```ruby
describe package('apache2') do
  it { should be_installed }
end

describe package('bundler') do
  it { should be_installed.by('gem').with_version('1.6.2') }
end
```

---

```ruby
describe port(80) do
  it { should be_listening.with('tcp') }
  it { should_not be_listening.with('udp6') }
end
```

---

```ruby
describe process("memcached") do
  it { should be_running }
  its(:args) { should match /-c 32000\b/ }
end
```
